//this program will ask the user how many hands they would like to be generated, then calculate the probability that the hands are part of a set
import java.util.Scanner;

public class Hw05{
  public static void main (String args[]){
    Scanner myScanner = new Scanner (System.in); //initializing Scanner
    //input variables 
    String cardValue1 = "";
    String cardValue2 = "";
    String cardValue3 = "";
    String cardValue4 = "";
    String cardValue5 = "";
    int handsToGenerate = 0; 
    int fourOfAKind= 0;
    int threeOfAKind= 0;
    int twoPair=0;
    int onePair=0;
   
    System.out.println("Please enter the number of hands you would like to generate: "); 
    //asks the user for how many hands they would like to be generated, and checks if the input is an integer
    while (!myScanner.hasNextInt()){
    System.out.println("Invalid entry, please enter an integer for the number of hands you would like to generate:");
   	myScanner.next();
     }//end of while 
    handsToGenerate=myScanner.nextInt();

    int x = 1; //counter variable
   
    while ( x <= handsToGenerate) { //this while loop will pick out 5 cards for each hand generated and assign it to a card value 

    int i = 1; //counter variable for inner while loop
    while (i < 6) {
    double cardPicked = (int)Math.ceil(Math.random() * 52); //this will generator a random number from 1 to 52
    int remainder= (int) cardPicked % 13; //this will divide the random number by 13 and store the remainder as an integer
    String cardFamily= ""; //what suit the card belongs to
    String cardValue =""; //what the card's value is
    //output variables 
    if (cardPicked>=1 & cardPicked<=13) { //if the random number generated is between 1 and 13  the card will belong to the Diamond suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break; //for example if the random number generated has a remainder of 0 that card's value will be a king
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement 
      cardFamily = "Diamonds"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=14 & cardPicked<=26) { //if the random number generated is between 14 and 26  the card will belong to the Clubs suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break; //for example if the random number generated has a remainder of 1 the card's value will be an Ace
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement  
      cardFamily = "Clubs"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=27 & cardPicked<=39){ //if the random number generated is between 27 and 39  the card will belong to the Hearts suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break; //for example if the random number generated has a remainder of 2 the card's value will be a 2
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement
      cardFamily = "Hearts"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=40 & cardPicked<=52){ //if the random number generated is between 40 and 52  the card will belong to the Spades suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break; //for example if the random number generated had a remainder of 10 the card's value will be a 10 
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement
      cardFamily = "Spades"; //declaring the card's suit
    }//end of this if statement
     //System.out.println("The " + i + " card you picked is: " + cardValue + " of " + cardFamily);
     //these if else statements assign the increments to variables that will be used later on 
     if (i == 1){
       cardValue1 = cardValue;
     }//end of if 1
     else if (i==2){
       cardValue2= cardValue;
     }//end of if 2
     else if (i==3){
       cardValue3= cardValue;
     }//end of if 3
     else if (i==4){
       cardValue4=cardValue;
     }//end of if 4
     else{
       cardValue5= cardValue;
     }//end of if 5
     i++;
   }//end of inner while loop
    
//the following if loops determines whether the cards picked fall into the categories of 4 of a kind, 3 of a kind, two pairs, or one pair 
      if ((cardValue1 == cardValue2 && cardValue2== cardValue3 && cardValue3== cardValue4) || (cardValue1 == cardValue2 && cardValue2== cardValue3 && cardValue3== cardValue5) ||(cardValue1 == cardValue2 && cardValue2== cardValue4 && cardValue4== cardValue5) || (cardValue1 == cardValue3 && cardValue3== cardValue4 && cardValue4== cardValue5) ||(cardValue2 == cardValue3 && cardValue3== cardValue4 && cardValue4== cardValue5) )
          {
       fourOfAKind++; 
      }//end of four of a kind loop
      else if ((cardValue1 == cardValue2 && cardValue2 == cardValue3) || (cardValue1==cardValue3 && cardValue3==cardValue4) || (cardValue1==cardValue4 &&cardValue4==cardValue5) || (cardValue1==cardValue2 && cardValue2==cardValue4) || (cardValue1==cardValue2 && cardValue2==cardValue5) || (cardValue1==cardValue3 && cardValue3==cardValue5) || (cardValue2==cardValue3 && cardValue3==cardValue4) || (cardValue2==cardValue3 && cardValue3==cardValue5) || (cardValue3==cardValue4 && cardValue4==cardValue5))
      {
        threeOfAKind++;
      }//end of three of a kind loop
      else if (((cardValue1==cardValue2) & (cardValue1==cardValue3)) || ((cardValue1==cardValue4) & (cardValue1==cardValue5)) || ((cardValue1==cardValue2) & (cardValue1==cardValue4)) ||((cardValue1==cardValue2) & (cardValue1==cardValue5)) ||((cardValue1==cardValue3) & (cardValue2==cardValue4)) ||((cardValue1==cardValue3) & (cardValue2==cardValue3)) || ((cardValue1==cardValue3) & (cardValue1==cardValue4)) || ((cardValue1==cardValue3) & (cardValue1==cardValue5)) || ((cardValue1==cardValue2) & (cardValue2==cardValue3)) || ((cardValue1==cardValue2) & (cardValue2==cardValue4)) || ((cardValue1==cardValue2) & (cardValue2==cardValue4)) || ((cardValue1==cardValue2) & (cardValue2==cardValue5)) || ((cardValue1==cardValue3) & (cardValue2==cardValue5)) || ((cardValue1==cardValue3) & (cardValue3==cardValue4)) || ((cardValue1==cardValue3) & (cardValue3==cardValue5)) || ((cardValue1==cardValue3) & (cardValue4==cardValue5)) || ((cardValue1==cardValue2) & (cardValue3==cardValue4)) || ((cardValue1==cardValue2) & (cardValue3==cardValue5)) || ((cardValue1==cardValue2) & (cardValue4==cardValue5)))
      {
        twoPair++;
      }//end of two pair loop
      else if ((cardValue1==cardValue2) || (cardValue1==cardValue3) || (cardValue1==cardValue4) || (cardValue1==cardValue5) || (cardValue2==cardValue3) || (cardValue2==cardValue4) || (cardValue2==cardValue5) || (cardValue3==cardValue4) || (cardValue3==cardValue5) || (cardValue4==cardValue5))
      {
        onePair++;
      }//end of one pair loop
    x++;
    
    }//end of outside while loop

    //outprint statements that determine the probability of the hands by dividing the occurances of the hands by the total number of hands
    System.out.println("The number of hands: " + handsToGenerate);
    System.out.printf("The probability of Four-of-A-Kind: %.3f\n " , ((double)((fourOfAKind)/(double)(handsToGenerate))));
    System.out.printf("The probability of Three-of-A-Kind: %.3f\n ", ((double)((threeOfAKind) /(double)(handsToGenerate))));
    System.out.printf("The probability of Two-Pair: %.3f\n" , ((double)((twoPair)/(double)(handsToGenerate))));
    System.out.printf("The probability of One-Pair: %.3f\n" , ((double)((onePair)/(double)(handsToGenerate))));
      
    
  }//end of main method 
  
}//end of class