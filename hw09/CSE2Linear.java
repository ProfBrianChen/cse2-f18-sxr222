//this program will perform a linear search 
import java.util.Scanner;
import java.util.*;


public class CSE2Linear{
  public static void main (String args[]) {
    
    Scanner myScanner = new Scanner(System.in);
    
		int numOfValues = 15;
		int arrayOfGrades[] = new int [numOfValues];

	
    System.out.print("Enter 15 ascending ints for final grades in CSE2: " + " : ");
   	 for (int i = 0; i < arrayOfGrades.length; i++) {
	   arrayOfGrades[i] = myScanner.nextInt();
		                                        }//end of for loop
		    
	  for (int i = 0; i < arrayOfGrades.length; i++) {
	    System.out.print(arrayOfGrades[i] + " ");
		                                         }//end of for loop
      
    System.out.print("Enter a grade to search for: ");
    int gradeToFind= myScanner.nextInt();
    
    int findGrade= binary(arrayOfGrades, gradeToFind);
    if (findGrade == -1){
	System.out.println(gradeToFind + " was not found in the list "); }//end of if statement
    else {
	System.out.println("Grade found at " + "index" + findGrade); }//end of else statement 
	
    System.out.print("Scrambled: ");
    scrambled(arrayOfGrades);
    printArray(arrayOfGrades);
    
    System.out.print("Enter a grade to search for: ");
    int gradeToFind2= myScanner.nextInt();
    int findGrade2= linear(arrayOfGrades, gradeToFind2);
    if (findGrade2 == -1){
	System.out.println(gradeToFind2 + " was not found in the list "); }//end of if statement
    else {
	System.out.println("Grade found at " + "index" + findGrade2); }//end of else statement 

  }//end of main method 
  
  public static int binary(int arrayOfGrades[], int gradeToFind) 
    { 
        int low = 0, high = arrayOfGrades.length - 1; 
        while (low <= high) 
        { 
            int m = low + (high-low)/2; 
            if (arrayOfGrades[m] == gradeToFind) 
                return m; 
            if (arrayOfGrades[m] < gradeToFind) 
                low = m + 1; 
            else
                high = m - 1; 
        } 
  
        return -1; 

    } //end of binary method

  public static void scrambled(int arrayOfGrades []){
	Random myRandom = new Random(); 
    for (int i = 0; i < arrayOfGrades.length; i++) { //uses the replace method 
        int scramble = myRandom.nextInt(arrayOfGrades.length);
        int temp = arrayOfGrades[i];
        arrayOfGrades[i] = arrayOfGrades[scramble];
        arrayOfGrades[scramble] = temp;
    }//end of for loop
}//end of method scramble

public static void printArray(int arrayOfGrades []){ 
  
     for (int j=0; j< arrayOfGrades.length; j++){ //iterate through the indeces and print out the value
      System.out.print( arrayOfGrades[j] + "  ");
    }//end of for loop
  }//end of method printarray

public static int linear(int arrayOfGrades [], int gradeToFind2){
	for (int i=0; i < arrayOfGrades.length; i++){
		if (arrayOfGrades[i]== gradeToFind2){
			return i; }//end of if statement
		}//end of for statement
		return -1;
}//end of linear method  
}//end of class