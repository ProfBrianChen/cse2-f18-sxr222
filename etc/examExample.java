public class examExample {
  public static void main( String args[]){
    int n = 8; 
    
    for (int i = 0; i < n; i++){
        for (int j=0; j <n; j++){
          if (i == j || j ==i+1 || j==i-1) {
            System.out.print("#");
          }
          else if (j == (n-1)-i || j==(n-1)-i+1 || j==(n-1)-i-1) {
            System.out.print("#");
          }
            
          else {
            System.out.print(".");
          }
        }
      System.out.print("\n");
    }
  }
}
//to deal with negative numbers and flipping the sign use
//if (n >0) system.out.print (#)
//if (n < 0) system.out.print(.)