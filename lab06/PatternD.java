//program to print out a pyramid thats inverted and goes from greatest to least 
import java.util.Scanner; //imports Scanner to use later 
public class PatternD{
	public static void main (String args[]){

		Scanner myScanner = new Scanner(System.in);
	
		System.out.print("Please print out an integer between 1 and 10 to represent the number of rows desired of the pyramid: "); //asks user for input 
		int rowNum;
		 	while (!myScanner.hasNextInt()){ //checks if that input is an integer
         			System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
   	    			myScanner.next();
                                       		       }//end of while 
   		rowNum=myScanner.nextInt();
   			while (rowNum > 10) { //checks if the input is in between 1 and 10 
			      System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
				    rowNum=myScanner.nextInt();
				                     }//end of while 
    //input variables    
    int i, j;
    for(i=rowNum; i>=1; --i) //prints out the pyramid, inverted with 1 on the bottom
        { 
                  
		      for(int k=i; k>=1; k--){ //prints out the value of each row from greatest to least
			        System.out.print(k + " ");
				  }//end of k
			System.out.println();   
          } //end of outer for loop
    }//end of main method
} //end of class