//this is a program to print an inverted number pyramid 
import java.util.Scanner; //import Scanner to be used in the program 

public class PatternB{
	public static void main (String args[]) {
		Scanner myScanner = new Scanner(System.in);
	
		System.out.print("Please print out an integer between 1 and 10 to represent the number of rows desired of the pyramid: "); //asks the user for the amount of rows desired
		int rowNum;
		 	while (!myScanner.hasNextInt()){ //checks if the user input is an integer
         			System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
   	    			myScanner.next();
                                       		       }//end of while 
   		rowNum=myScanner.nextInt();
			while (rowNum > 10) { //checks if the user input is in between 1 and 10 
			
         			System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
				rowNum=myScanner.nextInt();
				
                                       		       }//end of while 
		
		if ((rowNum > 0) & (rowNum < 11)) {

			for(int i = rowNum; i >= 1; --i) { //if there is a valid input, then the pyramid will be created, by decrementing i and then incrementing j until it is greater than i, then decrementing i until it is less than 1
           			 for(int j = 1; j <= i; ++j) {
                		 System.out.print(j + " ");
           			 }//end of inner for loop 
           		    System.out.println();
       			 }//end of for loop
		}//end of if loop
    
	}//end of main method

}//end of class