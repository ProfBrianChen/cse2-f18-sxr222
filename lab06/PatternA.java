 //this program will create a pyramid of numbers
import java.util.Scanner; //import scanner to be used 

public class PatternA{
	public static void main (String args[]) {
      Scanner myScanner = new Scanner(System.in);//initializing scanner 
	
	  System.out.print("Please print out an integer between 1 and 10 to represent the number of rows desired of the pyramid: "); //asks the user for number of rows
	  int rowNum=0; //rowNum to be used later
	while (!myScanner.hasNextInt()){ //checks for valid input, for an integer between 1 and 10 
         	System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
   	   myScanner.next();
                                  }//end of while 
   	rowNum=myScanner.nextInt();
    
	while (rowNum > 10) {	
         	System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
	rowNum=myScanner.nextInt();

                        }//end of while 
	

	if ((rowNum > 0) & (rowNum < 11)) { //if there is a valid input, then the pyramid will be created, by incrementing i and then incrementing j until it is greater than i, then incrementing i until it is greater than the rownumber inputted

	for(int i = 1; i <= rowNum; ++i) {
            	for(int j = 1; j <= i; ++j) {
                	System.out.print(j + " ");
            	}//end of inner for loop	
            	System.out.println();
        	}//end of for loop 
	}//end of if loop


	}//end of main method

}//end of class