//program to print out a pyramid that is pushed to the right 
import java.util.Scanner; //import Scanner to use
public class PatternC{
	public static void main (String args[]){

		Scanner myScanner = new Scanner(System.in);
	
		System.out.print("Please print out an integer between 1 and 10 to represent the number of rows desired of the pyramid: "); //asks user for imput
		int rowNum;
		 	while (!myScanner.hasNextInt()){ //checks if input is an integer
         			System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
   	    			myScanner.next();
                                       		       }//end of while 
   		rowNum=myScanner.nextInt();
   			while (rowNum > 10) { //checks if the input is between 1 and 10 
         			System.out.println("Invalid entry, please enter an integer for the number of rows desired:");
				rowNum=myScanner.nextInt();
				
                                       		       }//end of while 
        //input variables
        int i, j, k=2*rowNum-2, test=rowNum; 
        for(i=1; i<=test; ++i)  //increments number of rows
        { 
            for(j=0; j<=k; ++j)  //for the spaces to push the pyramid to the right
            { 
             System.out.print(" "); 
            } //end of inner for loop    
            k = k - 1;           
		  for(int x=i; x>=1; x--){ //to print out the values of each row from greatest to least 
			  System.out.print(x);
				  }//end of x
		  	System.out.println();

        } //end of outer for loop
    }//end of main method
} //end of class