//Swetha Ramesh
//CSE-002-311
//This program will compute the total cost of items bought at a store, including the PA sales tax of 6%
public class Arithmetic{
  public static void main(String[] args){
   
    //input data
    int numPants = 3; //total pair of pants bought
    double pantsPrice = 34.98; //cost of each pair of pants
    int numShirts = 2; //total number of sweatshirts bought
    double shirtPrice = 24.99; //cost of each sweatshirt
    int numBelts = 1; //total number of belts bought
    double beltCost = 33.99; //the price of the belt
    double paSalesTax = 0.06; //the sales tax rate in PA
    //intermediate variables 
    double totalCostOfPants; //total cost of pants bought
    double totalCostOfShirts; //total cost of sweatshirts bought
    double salesTaxOnPants; //sales tax on total purchase of pants
    double salesTaxOnShirts; //sales tax on purchase of sweatshirts
    double salesTaxOnBelt; //sales tax on belt
    double totalCostWithoutSales; //total cost of purchase without sales tax applied
    double totalSalesTax; //total cost of sales tax
    double totalCost; //total cost of the purchase with sales tax
    //calculations for output data
    totalCostOfPants = numPants * pantsPrice; //calculate the total cost of all the pants by multiplying the individual value by the number of pairs bought
    totalCostOfShirts = numShirts * shirtPrice; //calculate the total cost of shirts by multiplying the individual value by the number of shirts bought
    salesTaxOnPants = (int) ((totalCostOfPants * paSalesTax)*100); //calculate the sales tax charged buying all the pants by multplying the sales tax by the total cost of pantsPric
    salesTaxOnShirts = (int) ((totalCostOfShirts * paSalesTax)*100); //calculate the sales tax charged buying all the shirts by multiplyinh the sales tax by the total cost of shirts
    salesTaxOnBelt = (int) ((beltCost * paSalesTax)*100); //calculate the sales tax charged on the belt by multiplying the price of the belt by the sales tax
    totalCostWithoutSales = (int) ((totalCostOfPants + totalCostOfShirts + beltCost)*100); //calculate the cost of the purchase before tax
    totalSalesTax= (int) ((salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelt)); //calculate the total sales tax charged
    totalCost = ((totalCostWithoutSales + totalSalesTax) ); //calculate the final cost for the purchase
    //display results
    System.out.println("The total cost of the pants is $" + totalCostOfPants); // This displays the total cost of the pairs of pants bought
    System.out.println("The total cost of the sweatshirts is $" + totalCostOfShirts); // This displays the total cost of the sweatshirts bought
    System.out.println("The total cost of the belts is $" + beltCost); // This displays the total cost of the belts bought
    System.out.println("The total cost of the purchases before tax is $" + ( totalCostWithoutSales/100) ); //This displays the total cost of the shopping spree before tax
    System.out.println("The total sales tax on the pants is $" + (salesTaxOnPants/100) ); // This displays the sales tax on the pairs of pants bought
    System.out.println("The total sales tax on the sweatshirts is $" + (salesTaxOnShirts/100) ); // This displays the sales tax on the sweatshirts bought
    System.out.println("The total sales tax on the belts is $" + (salesTaxOnBelt/100) ); // This displays the sales tax on the belts bought
    System.out.println("The total sales cost on the items is $" + totalSalesTax/100 ); //This displays the total tax that will be added to the cost of the shopping spree
    System.out.println("The total cost of the purchases, including tax is $" + totalCost/100 ); //This displays the total cost of the shopping spree with the tax, the price you will pay at the register
 
  }//end of class
}//end of main method