//this program will create random sentences
import java.util.Random;
import java.util.Scanner;
public class lab07{
  public static void main( String args[])
{
    Scanner myScanner = new Scanner(System.in);
    Random myRandom = new Random();
    int tries=0;
    int  x= myRandom.nextInt(10);
    String adjWord = adjectives(x);
    String nounWord = nouns(x);
    String verbWord = verbs(x);
    String objectNounWord = objectNouns(x);
    String articleWord = articles(x);
    String articleWord2 = articles(x);
    String randomSentence = articleWord + " "+ nounWord + " " + verbWord +" "+ articleWord2 +" "+ objectNounWord ;
    System.out.println(randomSentence);
    String wordSentence2= actionSentences(nounWord);
    String actionSentence1 = wordSentence2 + " " + articleWord+ " " + wordSentence2+ " "+ verbWord + " " + adjWord + " " + articleWord + " " + objectNounWord;
    System.out.print(actionSentence1);
    do 
    {
    System.out.print("\nWould you like another sentence? yes/no ");
    String userChoice=myScanner.next();
    if (userChoice.equals("yes"))
	{ 
	 tries++;
	 //System.out.print(tries);
        int  y= myRandom.nextInt(10);    
    	String adjWord1 = adjectives(y);
    	String nounWord1 = nouns(y);
    	String verbWord1 = verbs(y);
    	String objectNounWord1 = objectNouns(y);
    	String articleWord1 = articles(y);
    	String articleWord21 = articles(y);
    	String randomSentence2 = articleWord1 + " "+ nounWord1 + " " + verbWord1 +" "+ articleWord21 +" "+ objectNounWord1 ;
	System.out.print(randomSentence2);
			 
       }//yes loop ends
    else {
	 System.out.print("Quit!!");
	 break;
         }//no loop ends 
  
     } while (tries>0);   
  }//end of main method

    
    public static String adjectives ( int randomInt) {
      
      int randomAdjective = randomInt;
      String adjective = "";
      switch (randomAdjective) { 
        case 0: adjective = "clumsy"; break; 
        case 1: adjective = "old"; break;
        case 2: adjective = "purple"; break;
        case 3: adjective = "skinny"; break;
        case 4: adjective ="amazing"; break;
        case 5: adjective= "blurry"; break;
        case 6: adjective = "delicious"; break;
        case 7: adjective ="funny"; break;
        case 8: adjective="grumpy"; break;
        case 9: adjective =" useful"; break;     
    } //end of this switch statement 
      return adjective;
    }//end of this method

    public static String nouns ( int randomInt){
      
      int randomNoun = randomInt;
      String noun = "";
      switch (randomNoun) { 
        case 0: noun = "airplane"; break; 
        case 1: noun = "badger"; break;
        case 2: noun = "caterpillar"; break;
        case 3: noun = "coffee"; break;
        case 4: noun ="rain"; break;
        case 5: noun = "teacher"; break;
        case 6: noun = "town"; break;
        case 7: noun ="words"; break;
        case 8: noun ="girl"; break;
        case 9: noun =" boy"; break;     
    } //end of this switch statement 
      return noun;
    }//end of this method

   public static String objectNouns ( int randomInt){
      //Random myRandom = new Random();
      int randomObjectNoun = randomInt;
      String  objectNoun = "";
      switch (randomObjectNoun) { 
        case 0: objectNoun = "professor"; break; 
        case 1: objectNoun = "restaurant"; break;
        case 2: objectNoun = "chocolate"; break;
        case 3: objectNoun = "car"; break;
        case 4: objectNoun ="laptop"; break;
        case 5: objectNoun = "movie"; break;
        case 6: objectNoun = "toy"; break;
        case 7: objectNoun ="drink"; break;
        case 8: objectNoun ="lizard"; break;
        case 9: objectNoun =" suit"; break;     
    } //end of this switch statement 
      return objectNoun;
    } //end of this method

  public static String verbs ( int randomInt){
      //Random myRandom = new Random();
      int randomVerb = randomInt;
      String verb = "";
      switch (randomVerb) { 
        case 0: verb = "walked"; break; 
        case 1: verb = "ran"; break;
        case 2: verb = "jumped"; break;
        case 3: verb = "burnt"; break;
        case 4: verb ="flew"; break;
        case 5: verb = "drove"; break;
        case 6: verb = "fell"; break;
        case 7: verb ="knew"; break;
        case 8: verb ="find"; break;
        case 9: verb ="ate"; break;     
    } //end of this switch statement 
      return verb;
    }//end of this method

  public static String articles (int randomInt){
	Random myRandom = new Random();
	int randomArticle = myRandom.nextInt(3);
	String article ="";
	switch (randomArticle) {
		case 0: article = "the"; break;
		case 1: article = "a"; break;
		case 2: article = "an"; break;
	}//end of this switch
	  return article;
    }//end of this method

  public static String actionSentences (String x){
       String y = x;
       Random myRandom = new Random();
	int randomActionWord = myRandom.nextInt(3);
	String wordForActionSentence="";
	switch (randomActionWord) {
		case 0: wordForActionSentence = "it"; break;
		case 1: wordForActionSentence= "this"; break;
		case 2: wordForActionSentence = "that"; break;
                                  }//end of switch
	return wordForActionSentence;


}//end of method actionSentences

  

//create another method called action, that passes the string subject noun and prints out the sentence with the action

}//end of class