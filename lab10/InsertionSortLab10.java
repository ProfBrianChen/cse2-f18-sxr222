//this program is for insertion sort
import java.util.Arrays;

public class InsertionSortLab10{
	public static void main(String [] args){
    int [] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9,};
    int [] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest= insertionSort(myArrayBest);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    int iterWorst = insertionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    
}//end of main

	//method for sorting

   public static int insertionSort (int [] list) {
	System.out.println(Arrays.toString(list)); //print out the original image
	
	int iterations =0;

	for (int i = 1; i <list.length; i++) {
		iterations++;
	for(int j =i; j > 0; j--) {
		if (list[j] < list[j-1]) { //if first index value is less than second, swap them, and then the next two indeces and etc. 
			int temp = list[j];
			list[j] = list[j-1];
			list[j-1] = temp;
			iterations++;
			
		}//end of if 
		
		 else {
			break;
		     }

	}//end of for
    
	System.out.println(Arrays.toString(list)); //print each iteration of the array
}//end of for		
 return iterations;
}//end of method insertionSort			

}//end of class