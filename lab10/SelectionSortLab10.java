//this program will test selection sort
import java.util.Arrays;

public class SelectionSortLab10 {
  public static void main( String[] args){
    int [] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9,};
    int [] myArrayWorst = {9,8,7,6,5,4,3,2,1};
    int iterBest= selectionSort(myArrayBest);
    System.out.println("The total number of operations performed on the sorted array: " + iterBest);
    int iterWorst = selectionSort(myArrayWorst);
    System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    
}//end of main

    //sorting method
    public static int selectionSort( int[] list) {
      System.out.println(Arrays.toString(list));
      int iterations =0;

      for (int i=0; i< (list.length -1); i++){
        iterations ++;
      
      int currentMin = list[i];
      int currentMinIndex = i;
      for (int j= i+1; j<list.length; j++){  //looks for min 
        if (list[j] < list[currentMinIndex]){
          currentMinIndex= j;
        }//end of if statement 
        iterations++;
      }//end of for statement  //after minimum value is found, it swaps the value
      if (currentMinIndex != i){
        int temp = list[currentMinIndex];
        list[currentMinIndex]=list[i];
        list[i]=temp; 
        System.out.println(Arrays.toString(list));
      }//end of if statement
      }//end of for
      return iterations; 
      
    }//end of method selectionSort
 
}//end of class