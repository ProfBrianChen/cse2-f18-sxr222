//This program will simulate the classic casino game, Craps. 
import java.util.Scanner; 
import java.lang.Math; 
//start of class
public class CrapsSwitch {
  //start of main method
  public static void main (String args[]) {
    //input
   Scanner myScanner = new Scanner (System.in); //initializing scanner
    System.out.print("If you wish to randomly cast dice please enter 0, if you wish to state two dice to evaluate please enter 1: "); //asks for user to decide how to input information
     int yourChoice = myScanner.nextInt();
     int firstDice = (int) ((Math.random() * 6) + 1);//generates random number from 1-6
     int secondDice = (int) ((Math.random() * 6) + 1);
     int sum = (int)(firstDice + secondDice); //sums the two numbers
     String slang =""; //string variable used later
    
      switch (yourChoice) {
        case 0: //case where random number is generated 
          switch (sum){ 
            case 2: slang="Snake Eyes"; break;//if the sum is 2 output snake eyes
            case 3: slang="Ace Deuce";  break;
            case 4:
              switch (firstDice){
                case 1: slang ="Easy Four"; break;//if the sum is 4 and the first dice rolled is 1, output easy four
                case 3: slang ="Easy Four"; break;
                case 2: slang="Hard Four"; break;
              }//end of case 4 switch 
            case 5: slang ="Fever Five"; break;
            case 6: 
              switch (firstDice) {
                case 1: slang =" Easy Six"; break;
                case 5: slang =" Easy Six"; break;
                case 2: slang =" Easy Six"; break;
                case 4: slang =" Easy Six"; break;
                case 3: slang= "Hard Six"; break;
              }//end of case 6 switch 
            case 7: slang = "Seven Out"; break;
            case 8:
              switch (firstDice){
                case 3: slang="Easy Eight"; break;
                case 5: slang="Easy Eight"; break;
                case 2: slang="Easy Eight"; break;
                case 6: slang="Easy Eight"; break;
                case 4: slang = "Hard Eight"; break;//if the sum is 8 and the first dice rolled is 4, output hard eight
              }//end of case 8 switch 
            case 9: slang =" Nine"; break;
            case 10:
              switch (firstDice){
                case 4: slang ="Easy Ten"; break;
                case 6: slang ="Easy Ten"; break;
                case 5: slang =" Hard Ten"; break;
              }//end of case 10 swtich 
            case 11: slang ="Yo-Eleven"; break;
            case 12: slang ="BoxCars"; break;
             // break;
          }
        break;
      
        case 1: { //case where numbers are inputted by the user 
           System.out.print("Please enter the value of the first dice roll: "); //asks user for first input
           int firstDiceRoll = myScanner.nextInt(); //stores that input into a variable
           System.out.print("Please enter the value of the second dice roll: "); // asks user for second input
           int secondDiceRoll = myScanner.nextInt();
           int sum1 = (firstDiceRoll + secondDiceRoll);
          switch (sum1){
              case 2: slang="Snake Eyes"; break; //if the sum is 2 output snake eyes
              case 3: slang="Ace Deuce";  break;
              case 4: 
              switch (firstDiceRoll) {
                case 1: slang ="Easy Four"; break;
                case 3: slang ="Easy Four"; break;
                case 2: slang="Hard Four"; break;
              }
            case 5: slang ="Fever Five"; break;
            case 6: 
              switch (firstDiceRoll){
                case 1:slang =" Easy Six";  break; 
                case 5:slang =" Easy Six";  break; 
                case 2:slang =" Easy Six";  break;
                case 4: slang =" Easy Six";  break;
                case 3: slang= "Hard Six"; break;
              }
            case 7: slang = "Seven Out"; break;
            case 8:
              switch (firstDiceRoll){
                case 3: slang="Easy Eight"; break;
                case 5: slang="Easy Eight"; break;
                case 2:  slang="Easy Eight"; break; //if the sum is 8 but one of the inputs is 2 , output easy eight 
                case 6: slang="Easy Eight"; break;
                case 4: slang = "Hard Eight"; break;
              }
            case 9: slang =" Nine"; break;
            case 10:
              switch (firstDiceRoll){
                case 4: slang ="Easy Ten"; break;
                case 6: slang ="Easy Ten"; break;
                case 5: slang =" Hard Ten"; break;
              }
            case 11: slang ="Yo-Eleven"; break;
            case 12: slang ="BoxCars"; break; 
            default: System.out.print(" You can only enter numbers between 1 and 6"); //checks to make sure it is in between 1 and 6
              System.exit(0);//leaves switch 
          }
          break;
      }
      } 
    // work for output
    System.out.println("You rolled " + slang);//outprint results 
  }//end of main method
}//end of class
    