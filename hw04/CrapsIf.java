//This program will simulate the classic casino game, Craps. 
import java.util.Scanner; 
import java.lang.Math; 
//start of class
public class CrapsIf {
  //start of main method
  public static void main (String args[]) {
    //input
   Scanner myScanner = new Scanner (System.in); //initializes the scanner 
    System.out.print("If you wish to randomly cast dice please enter 0, if you wish to state two dice to evaluate please enter 1: "); //asks user to pick a method to evaluate the dice
     int yourChoice = myScanner.nextInt();//takes input from user to evaluate the dice
     int firstDice = (int) ((Math.random() * 6) + 1); //if the user picks 0, this will create a random number from 1-6
     int secondDice = (int) ((Math.random() * 6)+1); 
     int sum = (firstDice + secondDice);  //this takes the sum of the two randomly created numbers
    // work for output
    if (yourChoice == 0) { //if the user picks 0 this if statement will start the process with randomly rolled dice
      if (sum == 2){ //if the sum of the two random dice is 2, it will outprint snake eyes
        System.out.print ( "You rolled Snake Eyes");
      } //end of sum 2 if statement 
      else if (sum == 3){ //if the sum if 3, it will outprint ace deuce
        System.out.print(" You rolled Ace Deuce");
      }//end of sum 3 if statement 
       else if (sum == 4){ //if the sum is 4...
         if (firstDice==1){ //and the dice rolled is either a 1 or 3 it will print out easy four
        System.out.print( "You rolled Easy Four");
         }//end of easy four if statement 
         if (firstDice==3){
        System.out.print( "You rolled Easy Four");
         }//end of easy four if statement 
         if (firstDice==2 ) { //but if the dice rolled is a 2, it will print out hard four 
          System.out.print( "You rolled Hard Four"); 
         }//end of hard four if statement 
      }//end of sum 4 if statement 
       else if ( sum == 5 ) { //if the sum is 5 it will outprint fever five
        System.out.print( "You rolled Fever Five");
      }//end of sum 5 if statement 
       else if ( sum ==6)  {//if the sum is a 6....
         if (firstDice==1) { //if either of the dice rolled is a 1, 5 , 2, 4 it will outprint easy six
           System.out.print( "You rolled Easy Six");
         }//end of easy six if statement 
         if (firstDice==5){
        System.out.print( "You rolled Easy Six");
         }//end of easy six if statement 
         if (firstDice==2){
        System.out.print( "You rolled Easy Six");
         }//end of easy six if statement 
         if (firstDice==4){
        System.out.print( "You rolled Easy Six");
         }//end of easy six if statement 
        else if (firstDice==3){ //but if either of the dice rolled is a 3, it will outprint hard six 
          System.out.print( "You rolled Hard Six");
        }//end of hard six if statement 
      }//end of sum 6 if statement 
       else if ( sum == 7 ){ //if the sum is 7, it will outprint seven out 
        System.out.print( "You rolled Seven Out");
      }//end of sum 7 if statement 
       else if (sum == 8){ //if the sum is 8...
          if (firstDice==3) {//if either of the dice rolled is a 3, 5, 2, 6 it will outprint easy eight
             System.out.print( "You rolled Easy Eight");
          }//end of easy eight if statement 
         if (firstDice==5){
        System.out.print( "You rolled Easy Eight");
         }//end of easy eight if statement 
         if (firstDice==2){
        System.out.print( "You rolled Easy Eight");
         }//end of easy eight if statement 
         if (firstDice==6){
        System.out.print( "You rolled Easy Eight");
         }//end of easy eight if statement 
          else if (firstDice==4 ){//but if either of the dice rolled is a 4, it will outprint hard eight 
            System.out.print("You rolled Hard Eight");
          }//end of hard eight if statement 
      }//end of sum 8 if statement 
      else if ( sum == 9) {//if the sum is 9 it will outprint nine
        System.out.print( "You rolled Nine");
      }//end of sum 9 if statement 
       else if (sum == 10){ //if the sum is 10...
         if( firstDice==5 ){//if either of the dice rolled is a 5 it will outprint hard ten 
           System.out.print("You rolled Hard Ten");
         }//end of hard ten if statement
         if (firstDice==6){//but if either of the dice rolled is a 6 or a 4 it will outprint easy ten 
        System.out.print( "You rolled Easy Ten");
         }//end of east ten if statement 
         else if (firstDice==4) {
           System.out.print("You rolled Easy Ten"); // add every combination like first dice == 6
         }//end of easy ten if statement 
      }//end of sum 10 if statement 
       else if ( sum == 11 ){//if the sum is 11 it will print out yoeleven
        System.out.print( "You rolled Yo-Eleven");
      }//end of sum 11 if statement 
       else if (sum == 12) { //if the sum is 12 it will print out boxcar
        System.out.print( "You rolled Boxcars");
      }//end of sum 12 
      
    }//end of main if statement 
     
    
    else if (yourChoice == 1) { //this will take input from the user
      System.out.print("Please enter the value of the first dice roll: "); //asks the user to input a number for the dice
      int firstDiceRoll = myScanner.nextInt(); //stores that number into a variable
      System.out.print("Please enter the value of the second dice roll: "); 
      int secondDiceRoll = myScanner.nextInt();
      double sum1 = (int)firstDiceRoll + secondDiceRoll; //sums the two variables
   
      if (sum1 == 2){//if the sum is 2 it will outprint snake eyes
        System.out.print ( "You rolled Snake Eyes");
      }//end of sum 2 if statement 
      else if (sum1 == 3){ //if the sum is 3 it will out print ace deuce
        System.out.print(" You rolled Ace Deuce");
      }//end of sum 3 if statement 
       else if (sum1 == 4){ //if the sum is 4, depending on the conditions, it will outprint different slang
         if (firstDiceRoll==1 ){
        System.out.print( "You rolled Easy Four");
         }//end of easy four if statement 
         else if (firstDiceRoll == 3) {
          System.out.print( "You rolled Easy Four"); 
         }//end of easy four if statement 
         if (firstDiceRoll == 2) {
          System.out.print( "You rolled Hard Four"); 
         }//end of hard four if statement 
      }//enf of sum 4 statement 
       else if ( sum1 == 5 ) {//if the sum is 5 it will print 5
        System.out.print( "You rolled Fever Five");
      }//end of sum 5 if statement 
       else if ( sum1 ==6)  {//if the sum is 6, depending on the different inputs it will print out different slang
         if (firstDiceRoll==1) {
           System.out.print( "You rolled Easy Six");
         }//end of easy six if statement
         else if (firstDiceRoll == 5) {
          System.out.print( "You rolled Easy Six"); 
         }//end of easy six 
         else if (firstDiceRoll == 2) {
          System.out.print( "You rolled Easy Six"); 
         }//end of easy six if statement 
        else if (firstDiceRoll == 4) {
          System.out.print( "You rolled Easy Six"); 
         }//end of easy six if statement 
        else if (firstDiceRoll==3 ) {
          System.out.print( "You rolled Hard Six");
        }//end of hard six if statement 
      }//end of sum 6 if statement 
       else if ( sum1 == 7 ){ //if the sum is 7 it will print sevenout 
        System.out.print( "You rolled Seven Out");
      }//end of sum 7 if statement 
       else if (sum1 == 8){//if the sum is 8, depending on the input it will print out different slang
          if (firstDiceRoll==3) {
             System.out.print( "You rolled Easy Eight");
          } //end of easy eight if statement 
          else if (firstDiceRoll ==5) {
          System.out.print( "You rolled Easy Eight"); 
         }//end of hard eight if statement 
         else if (firstDiceRoll == 2) {
          System.out.print( "You rolled Hard Eight"); 
         }//end of hard eight if statement 
         else if (firstDiceRoll == 6) {
          System.out.print( "You rolled Hard Eight"); 
         }//end of hard eight if statement 
          else if (firstDiceRoll==4 ){
            System.out.print("You rolled Hard Eight");
          }//end of hard eight if statement 
      }//end of sum 8 if statement 
      else if ( sum1 == 9) {//if the sum is 9 it will print out nine
        System.out.print( "You rolled Nine");
      }//end of sum 9 if statement 
       else if (sum1 == 10){ //if the sum is 10 it will outprint different slang depending on the dice roll
         if( (firstDiceRoll==5 ) ){
           System.out.print("You rolled Hard Ten");
         }//end of hard ten if statement 
         else if (firstDiceRoll==4 ) {
           System.out.print("You rolled Easy Ten");
         }//end of easy ten if statement 
         else if (firstDiceRoll == 6) {
          System.out.print( "You rolled Easy Ten"); 
         }//end of easy ten if statement 
      }//end of sum 10 if statement 
       else if ( sum1 == 11 ){//if the sum is 11 it will out print yoeleven
        System.out.print( "You rolled Yo-Eleven");
      }//end of sum 11 if statement 
       else if (sum1 == 12) {//if the sum is 12 it will out print boxcars
        System.out.print( "You rolled Boxcars");
      } //end of sum 12 if statement 
       else if (firstDiceRoll > 6 | secondDiceRoll > 6){ //if the values exceed 6 for any of the dice value, it will print an error
         System.out.print("Invalid entry, dice values cannot exceed 6");
       }//end of invalid if statement 
    }//end of main if statement 
      
    
  }//end of class
}//end of class