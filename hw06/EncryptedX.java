//this program will create an encrypted x depending on the user input

import java.util.Scanner; //import Scanner to use in code 

public class EncryptedX{
	public static void main (String args[]) {

		Scanner myScanner = new Scanner(System.in); //initialize Scanner
	
		System.out.print("Please print out an integer between 1 and 100 to represent the size of the square: "); //ask user for input and then validates the input
		int size=0;
		 	while (!myScanner.hasNextInt()){ //checks if input is an integer
         			System.out.println("Invalid entry, please enter an integer for the size of the square:");
   	    			myScanner.next();
                                       		       }//end of while 
   		size=myScanner.nextInt();
			while (size > 100) { //makes sure that integer is not greater than 100 
			
         			System.out.println("Invalid entry, please enter an integer between 1 and 100 to represent the size of the square:");
				size=myScanner.nextInt();
				
                                       		       }//end of while 

for (int row = 1; row<=size; row++)  //creates rows     
     {            
		for (int col=1; col<=size; col++)    //creates columns         
			{            
                      if ( (row == col)|| (col == (size - (row-1)) ) ) // this will create the x down the middle, printing out a space instead of a star
		{      
        		 System.out.print(" ");  
 		} //end of nested if              
        		 else   {              
        			 System.out.print("*");    
				 }   //end of else                        
        	}//end of inner for loop 
        		System.out.println();       
    }  //end of outer for loop 
}// end of main 
}//end of class