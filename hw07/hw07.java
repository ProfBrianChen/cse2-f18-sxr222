//this is code for hw07
import java.util.Scanner; //import scanner to use later
import java.io.*; //import string functions to use later

public class hw07{

public static String sampleText(){ //method that asks user for a sample text, then prints that sample text out
			Scanner myScanner = new Scanner(System.in);

			System.out.println("Enter a sample text: ");
			String input = myScanner.nextLine();
			return input;
						  }//end of method sampleText
  
public static void printMenu() { //method that prints out a menu for the user to pick from 
	Scanner myScanner =new Scanner(System.in);
		
		System.out.println("MENU\n c - Number of non-whitespace characters\n w - Number of words\n f - Find text\n r - Replace all !'s\n s - Shorten spaces\n q - Quit\n Choose an option:" );
									

}//end of method printMenu
		
	public static int getNumOfNonWSCharacters(String input) { //method that looks at the sample text and determines how many characters there are, excluding spaces
		int spaces = 0;
		int inputLength;
		int i = 0;
		String output = input;
		for( inputLength=output.length(); i <= (inputLength-1); i++){
			if (output.charAt(i) == ' '){
					spaces++;    } //end of if
			}//end of for
		inputLength = ((output.length()) - spaces);
		return inputLength;
								      }//end of getNumOfNonWSCharacters method

	public static int getNumOfWords(String input) { //method that looks at the sample text and counts the number of words
	    int count = 0;
	    String output = input;		
	    for(int i = 0; i < output.length(); i++){
            if(output.charAt(i) == ' '){
                count++;
                } //end of if
            }//end of for
        
        return (count+1);
							}// end of getNumOfWords method

	public static int findText(String input, String search){ //method that asks user for a word that they would like to find, then finds that word in the sample text
		String searchWord = search;
		int count = 0;
		String output = input;
		while (output.contains(search)) {
			count++;
		output= output.substring(output.indexOf(search)+search.length());
		} //end of while
						
	        return count;
}

	public static String replaceExclamation( String input){ //method that will look at the sample text provided and replace exclamation marks with spaces
		
		String output = input;
		String newOutput= output.replace("!", " ");       		
			return newOutput;	
			} //end of replaceExclamation method
	
	public static String shortenSpace(String input){ //method that will look at the sample text and change excessive spaces 
	       
	       String output = input;
	       String newOutput= output.replace("  ", " ");
		return newOutput;
}//end of shortenspace method 
			
		
	public static void main(String args[]){
		Scanner myScanner = new Scanner(System.in);	
                String x = sampleText(); //calls method 
		System.out.println("You entered: " + x); //returns the entered text
		printMenu();

		while (true) //always true 
		{
			if (myScanner.hasNext()==true)//if the input is a string
			{
				
				String input = myScanner.next();
				System.out.print("Your choice is: " + input + "\n");
				if (input.contains("c"))//if the user picks c, then method getNumOfNonWSCharacters will be called
				{
					System.out.println("non-whitespace characters: " + getNumOfNonWSCharacters(x));
					break; 
				}//end of if
				
				else if(input.contains("w"))//if the user picks w, then method getNumOfWords will be called
				{
					System.out.println("Word count: " + getNumOfWords(x));
					break;
				} //end of if
				else if (input.contains("f")) //if the user picks f, then method findText will be called
				{
					Scanner myScanner2 = new Scanner(System.in);
					System.out.print("Enter the word you wish to search for: ");
					String wordToFind = myScanner2.next();
					System.out.println( " instances: " + findText(x, wordToFind));
					break;
				} //end of if
				else if (input.contains("r")) //if the user picks r, then the method replaceExclamation will be called
				{
					System.out.println("New Text: " + replaceExclamation(x));
					break; 
				}//end of if
				else if (input.contains("s")) //if the user picks s, then the method shortenSpace will be called
				{
					System.out.println("New text: " + shortenSpace(x));
					break;
				} //end of if
				else if (input.contains("q")) //if the user picks q then the progrom will quit
				{
					break;
				} //end of if
			printMenu();
			}//end of main if
		} //end of while 



}//end of main method

}//end of class