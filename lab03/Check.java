//This program will use input from the user. The user will input the original cost of the check, the percentage tip they want to pay, and the number of ways the check 
//needs to be split. The program will then determine how much each person needs to spend in order to pay the check. 
import java.util.Scanner; 
//start of class
public class Check{
  public static void main(String args[]) {
    Scanner myScanner = new Scanner (System.in); //declaring instance of scanner
    //input variables
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts user to enter original cost of the check
    double checkCost = myScanner.nextDouble(); //sets value entered by user as variable checkCost;
    System.out.print("Enter the percentage of tip you wish to pay as a whole number (in the form xx): "); //prompts user to enter the tip they wish to pay
    double tipPercent = myScanner.nextDouble(); //sets value entered by user as variable tipPercent
    tipPercent/=100; //conver % into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //prompts user to enter the amount of people who will split the check
    int numPeople = myScanner.nextInt();
    //output variables
    double totalCost; //variable for total cost of the dinner, including tips
    double costPerPerson; //variable for amount each person who went out to dinner will pay
    int dollars, dimes, pennies; // dollar amount for cost, and cents values for the right of the decimal point
    totalCost = checkCost*(1+ tipPercent); //determines the total cost of the dinner by multiiplying the original cost of the check with the tip percentage
    costPerPerson = totalCost / numPeople; //determines the dollar value each person will pay for the meal
    dollars = (int)costPerPerson; //changes the cost per person to just the dollar value
    dimes = (int) (costPerPerson * 10) % 10; //determines the cents, specifically the tens place (dimes) value each person will pay for the meal
    pennies = (int) (costPerPerson *100) % 10; //determines the cents, specifically the ones place (pennies) value each person will pay for the meal 
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //shows the user what each person will pay for the meal 
    
  } //end of main method
} //end of class
  