//this program will deal with one dimensional arrays
import java.util.Arrays;
public class lab08{
  public static void main( String args[]){
    //variable declaration 
    int [] array1;
    int [] array2;
    int arraySize=100;
    array1 = new int[arraySize];
    array2= new int[arraySize];
    
    //assigns 100 random variables into array 1
    System.out.print("Array 1 contains this: ");
    for (int i =0; i < arraySize; i++){
      int values = (int )(Math.random() * 49 + 1);
      array1[i]=values;   
    }
    //prints out array1
    for (int j=0; j<arraySize; j++){
      System.out.print( array1[j] + " ");
    }
    //prints out a space inbetween array1 and array2
    System.out.println();
    
Arrays.sort(array1); //sorts the numbers in array1 from least to greatest
    for (int n: array1){ //goes through each index of the array to see if there are repeating arrays
      array2[n]++;
    }
    for (int j=0; j<(arraySize);j++){ //prints out array 2, the number only prints out if that number occurs at least once
      if(array2[j]>=1){
        System.out.println(j +" occurs " + array2[j] + " times");
      }
    }
  }//end of main method
  
}//end of class