//this program will ask the user to enter information regarding the course they are taking
import java.util.Scanner; //imports Scanner
public class Course {
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in); //declaring instance of scanner
    //variable declaration 
   int courseNumber=0; //for course number
   String departmentName = "";// for department name
   int classMeets = 0; //for how many times the class meets
   int classStart = 0; //for what time the class starts
   String instructorName = ""; //for the instructor's name
   int numStudents = 0; //for the number of students in the class
    
   System.out.println("What is the course number? "); 
    //asks the user for the course number, the following while loop will reask the question if the first input is an invalid input 
       while (!myScanner.hasNextInt()){
         System.out.println("Invalid entry, please enter an integer for the course number:");
   	     myScanner.next();
                                       }//end of while 
    courseNumber=myScanner.nextInt();
    
    System.out.println ("What is the department name?\n"); //asks the user for the department name
    //asks the user for the department name, the following while loop will reask the question if the first input is an invalid input 
       while (!myScanner.hasNextLine()){
         System.out.println("Invalid entry, please enter an String for the department name:");
   	     myScanner.next();
                                       }//end of while 
    departmentName=myScanner.next();
    
    System.out.print("How many times a week does the class meet? \n"); //asks the user for the number of times a week the class meets
     while (!myScanner.hasNextInt()){
         System.out.println("Invalid entry, please enter an integer for how many times a week the class meets:");
   	     myScanner.next();
                                       }//end of while 
    classMeets=myScanner.nextInt();
    
    System.out.print("What time does the class start? \n"); //asks user for the start times
     while (!myScanner.hasNextInt()){
         System.out.println("Invalid entry, please enter an integer for what time the class starts:");
   	     myScanner.next();
                                       }//end of while 
    classStart=myScanner.nextInt();
    
    System.out.print("What is the instructor's name?\n"); //asks the user for the instructor's name
     while (!myScanner.hasNextLine()){
         System.out.println("Invalid entry, please enter a String for the instructor's name");
   	     myScanner.next();
                                       }//end of while 
    instructorName=myScanner.next();
    
   System.out.print("How many students are in the class?\n"); //asks the user for the total amount of students
     while (!myScanner.hasNextInt()){
         System.out.println("Invalid entry, please enter an integer for how many students are in the class:");
   	     myScanner.next();
                                       }//end of while 
    numStudents=myScanner.nextInt();
    
  }//end of main method
}//end of class