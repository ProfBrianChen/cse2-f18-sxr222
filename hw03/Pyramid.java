//This program will ask the user for the dimensions of a pyramid, and then return the volume of that pyramid. 
import java.util.Scanner;
//start of class
public class Pyramid{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in); //declaring the instance of the scanner
    //input variables
    System.out.print("The square side of the pyramid is (input length) : "); //prompts user to enter the dimensions of the square side of the pyramid
    double pyramidLength = myScanner.nextDouble(); //stores that input as a variable
    System.out.print("The height of the pyramid is (input height): "); // prompts user to enter the height of the pyramid
    double pyramidHeight = myScanner.nextDouble(); //stores that input as a variable
    //calculation  and output variables
    double squareArea = Math.pow(pyramidLength, 2); // finds the area of the base of the pyramid, which is a square, using length^2
    double pyramidVolume = (( squareArea * pyramidHeight) / 3) ; //finds the volume of the pyramid by multiplying the area of the base of the pyramid with the height, then divinding it all by 3
    System.out.printf("The volume inside the pyramid is: %.2f\n", pyramidVolume ); // displays the volume of the pyramid to the tenths plance   
  }//end of main method
} //end of class