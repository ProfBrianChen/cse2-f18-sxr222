//This program will ask the user to input doubles that represent the number of acres of land affected by hurricane precipition
//and it will ask the user to input how many inches of rain were dropped on average.
//The program will then return the quantity of rain in cubic miles.
import java.util.Scanner;
//start of class
public class Convert{
  public static void main (String args[]) {
    Scanner myScanner = new Scanner (System.in); //declaring the instance of the scanner
    //input variables
    System.out.print("Enter the affected area in acres: "); //promts user to enter the number of acres of land affected by hurrican precipition
    double affectedArea = myScanner.nextDouble(); //stores that input as a variable
    System.out.print("Enter the rainfall in the affected area (in inches): "); // prompts the user to enter the inches of rain that were dropped on average 
    double averageRain = myScanner.nextDouble(); //stores that input as a variable
    //conversion variables
    double gallonsPerAcre = 27154.00; //one inch of rain over one acre of land is equivalent to 27,154 gallons of water
    double gallonsPerCubicMile = 9.08169e-13; //one gallon of water is equivalent to 9.08168 x 10^-13 cubic miles
    //output variables
    double amountInGallons = affectedArea * gallonsPerAcre * averageRain; //this will convert the inches of water on the acres of land to total gallons of water
    double amountInCubicMiles = amountInGallons * gallonsPerCubicMile; //this will ocnvert the total gallons of water to quantity of rain in cubic miles
    System.out.printf("%f cubic miles\n ", amountInCubicMiles ); //displays the results to user
   
  } //end of main 
} //end of class