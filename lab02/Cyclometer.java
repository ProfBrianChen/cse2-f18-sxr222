//Swetha Ramesh
//CSE-002-311
//September 6, 2018
//This program should  imitate a bicycle cyclometer. 
//It should print the number of minutes and counts for each trip.the distance of each trip and the combined distance of two trips

public class Cyclometer{
  //main method
  public static void main(String[] args){
    
    //input data
    int secsTrip1=480; //trip 1 was a total of 480 seconds
    int secsTrip2=3220; //trip 2 was a total of 3220 seconds
    int countsTrip1=1561; //the front wheel rotated 1561 times in the first trip
    int countsTrip2=9037; //the front wheel rotated 9037 times in the second trip
    //intermediate variables and output data
    double wheelDiameter=27.0; //the diameter of the front wheel of the bike is 27 inches
    double PI= 3.14159; //need pi for calaculations
    int feetPerMile=5280; //conversion needed for calculations
    int inchesPerFoot=12; //conversion needed for calculations
    double secondsPerMinute=60; //conversion needed for calculations
    double distanceTrip1, distanceTrip2, totalDistance; //this is the distance the bike traveled in trip 1, trip 2, and the total comibined distance of both trips
    //displaying data
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+ " minutes and had " + countsTrip1 +" counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute)+ " minutes and had " + countsTrip2 +" counts.");
    //compute value for distances by converting the counts from the wheel to inchesperfoot, then converting it to miles
    distanceTrip1=countsTrip1*wheelDiameter*PI; //converts wheel diameter to distance in inches, for each count of the wheel, the rotation of the wheel is diameter in inches times pi
    distanceTrip1/=inchesPerFoot*feetPerMile; // gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//converts counts from trip 2 to distance traveled in miles
	  totalDistance=distanceTrip1+distanceTrip2; // combines trip 1's distance and trip 2's distance 
    //print out calculations
    System.out.println("Trip 1 was "+ distanceTrip1+ " miles");
	  System.out.println("Trip 2 was "+ distanceTrip2+ " miles");
	  System.out.println("The total distance was "+ totalDistance+ " miles");
 
  }//end of class
}//end of main method