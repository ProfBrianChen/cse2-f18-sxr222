//this program is for lab 09
import java.util.*;
public class lab09{
  
  public static int [] copy( int [] array1){
    
    int []array2 = new int[array1.length];
      for (int i=0; i < array1.length; i++){
        array2[i]=array1[i];
      }//end of for loop
    return array2;
  }//end of method copy
  
  public static void inverter(int [] array1){
    for (int i=1; i < array1.length/2; i++){
       int temp = array1[i];
       array1[i] = array1[array1.length - i ];
       array1[array1.length - i ] = temp;
      
    }//end of for loop
  }//end of method inverter
  
  public static int [] inverter2(int [] array1){
    int [] array2= copy(array1);
     for (int i=1; i < array2.length/2; i++){
       int temp = array2[i];
       array2[i] = array2[array1.length - i ];
       array2[array1.length - i ] = temp;
      
    }//end of for loop
    return array2;
  }//end of method inverter2
  
  public static void print(int [] array1){
    for (int i=1; i < array1.length; i++){
      System.out.print(array1[i] + " ");
      
    }//end of for loop
  }//end of method print
  
  public static void main( String [] args){
    int [] array0 = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    int [] array1= copy(array0);
    int [] array2 = copy(array0);
    
    
    inverter(array0);
    print(array0);
    System.out.println();
    
    inverter2(array1);
    print(array1);
    System.out.println();
    
    int [] array3= inverter2(array2);
    print(array3);
  }//end of method main
}//end of class