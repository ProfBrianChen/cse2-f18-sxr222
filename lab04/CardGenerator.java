//This program will use a random number generator to pick a random card from a deck so that a magician can practice his tricks alone. 
import java.lang.Math; //calls the method with the random generator in it
//start of class
public class CardGenerator{
  //start of method
  public static void main (String args[]) {
    //declaring input variables
    double cardPicked = (int)Math.ceil(Math.random() * 52); //this will generator a random number from 1 to 52
    int remainder= (int) cardPicked % 13; //this will divide the random number by 13 and store the remainder as an integer
    String cardFamily= ""; //what suit the card belongs to
    String cardValue =""; //what the card's value is
    //output variables 
    if (cardPicked>=1 & cardPicked<=13) { //if the random number generated is between 1 and 13  the card will belong to the Diamond suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break; //for example if the random number generated has a remainder of 0 that card's value will be a king
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement 
      cardFamily = "Diamonds"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=14 & cardPicked<=26) { //if the random number generated is between 14 and 26  the card will belong to the Clubs suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break; //for example if the random number generated has a remainder of 1 the card's value will be an Ace
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement  
      cardFamily = "Clubs"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=27 & cardPicked<=39){ //if the random number generated is between 27 and 39  the card will belong to the Hearts suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break; //for example if the random number generated has a remainder of 2 the card's value will be a 2
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break;
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement
      cardFamily = "Hearts"; //declaring the card's suit
    }//end of this if statement 
    else if (cardPicked>=40 & cardPicked<=52){ //if the random number generated is between 40 and 52  the card will belong to the Spades suit
      switch (remainder) { //this will look at the remainder of the random number and assign that value to a corresponding card value 
        case 0: cardValue = "King"; break;
        case 1: cardValue = "Ace"; break;
        case 2: cardValue = "2"; break;
        case 3: cardValue = "3"; break;
        case 4: cardValue ="4"; break;
        case 5: cardValue = "5"; break;
        case 6: cardValue = "6"; break;
        case 7: cardValue ="7"; break;
        case 8: cardValue ="8"; break;
        case 9: cardValue =" 9"; break;
        case 10: cardValue = "10"; break; //for example if the random number generated had a remainder of 10 the card's value will be a 10 
        case 11: cardValue = "Jack"; break;
        case 12: cardValue = "Queen"; break; 
    } //end of this switch statement
      cardFamily = "Spades"; //declaring the card's suit
    }//end of this if statement 
    //displaying the results 
    System.out.println("You picked the " + cardValue + " of " + cardFamily);
    
  } //main method end
} //end of class